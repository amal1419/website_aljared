from django.apps import AppConfig


class AljaredAppConfig(AppConfig):
    name = 'aljared_app'
